<?php 

	public $uses = array(
		'GeneoStandard',
		'GeneoJes',
		'GeneoStep',
		// added missing models:			
		'GeneoProcessFlow',
		'VisualAid',
		'GeneoFileUpload'
	);

	public function index() {
	
		// Code trimmed 

		$this->GeneoStandard->cacheQueries = false;
		$this->GeneoJes->cacheQueries = false;
		$this->GeneoStep->cacheQueries = false;
		// added missing models:	
		$this->GeneoProcessFlow->cacheQueries = false;
		$this->VisualAid->cacheQueries = false;
		$this->GeneoFileUpload->cacheQueries = false;

		$preResults = array_merge(
			$this->GeneoStandard->getSearchData($query),
			$this->GeneoJes->getSearchData($query),
			$this->GeneoStep->getSearchData($query),
			// added missing models after writting a getSearchData() implementation for them:
			$this->GeneoProcessFlow->getSearchData($query),
			$this->VisualAid->getSearchData($query),
			$this->GeneoFileUpload->getSearchData($query) 

		);
		// Code trimmed 
	}
