<?php 

/**
 * Accepts a search term and returns results for front-end searches
 *
 * @param string $term the term to search for
 * @param array $conditions additional conditions
 * @return array Search results
 */
	public function getSearchData($term, $conditions = array()) {
		$this->setDataSource('default');
		$containers = array(
			'GeneoDepartment',
			'GeneoLocation',
			'GeneoWorkSpace'
		);

		$geneoFileUploads = array();

		$searchTerm = $this->getDataSource()->getConnection()->quote($term);

		foreach ($containers as $container) {
			foreach (GeneoAuth::$statuses as $key => $status) {
				if ($key == GeneoAuth::$statusAll || $key == GeneoAuth::$statusPrevious) {
					continue;
				}
				
				$options = $this->paginateOptions($key, $container, false);
				if ($options == false) {
					continue;
				}
				
				$fullText = "MATCH(GeneoFileUpload.name, GeneoFileUpload.description, GeneoFileUpload.draft_title) AGAINST({$searchTerm} IN BOOLEAN MODE)";
				$this->virtualFields['fulltext_score'] = $fullText;
				$number = ltrim(substr($term, 0, -1), '0');

				if (is_numeric($number)) {
					$options['conditions'][] = array(
						'OR' => array(
							$fullText,
							'GeneoFileUploadCollection.document_number' => $number
						)
					);
				} else {
					$options['conditions'][] = $fullText;
				}
				
				$options['fields'] = array(
					'GeneoFileUpload.id',
					'GeneoFileUpload.status_id',
					'GeneoFileUpload.name',
					'GeneoFileUpload.description',
					'GeneoFileUpload.draft_title',
					'GeneoFileUpload.version',
					'GeneoFileUpload.created',
					'GeneoFileUpload.modified',
					'GeneoFileUpload.issue_date',
					'GeneoFileUpload.fulltext_score',
					'GeneoFileUpload.geneo_file_upload_collection_id',
					'GeneoFileUploadCollection.document_number',
				);

				$options['limit'] = 20;

				
				$geneoFileUploads = array_merge($geneoFileUploads, $this->find('all', $options));

			}
		}

		$results = array();

		foreach ($geneoFileUploads as $geneoFileUpload) {

			$number = sprintf("%'06s", $geneoFileUpload['GeneoFileUploadCollection']['document_number']);

			$link = array(
				'controller' => 'geneo_file_uploads',
				'action' => 'view',
				$geneoFileUpload['GeneoFileUpload']['id']
			);

			$results[] = array(

				'hit' => 'File Upload',
				'score' => $geneoFileUpload['GeneoFileUpload']['fulltext_score'],
				'documents' => array(
					'File Upload' => array(
						'type' => 'File Upload',
						'number' => $number,
						'id' => $geneoFileUpload['GeneoFileUpload']['id'],
						'status' => GeneoAuth::getName($geneoFileUpload['GeneoFileUpload']['status_id']),
						'status_id' => $geneoFileUpload['GeneoFileUpload']['status_id'],
						'name' => $geneoFileUpload['GeneoFileUpload']['name'],
						'description' => $geneoFileUpload['GeneoFileUpload']['description'],
						'draft_title' => $geneoFileUpload['GeneoFileUpload']['draft_title'],
						'version' => $geneoFileUpload['GeneoFileUpload']['version'],
						'created' => $geneoFileUpload['GeneoFileUpload']['created'],
						'modified' => $geneoFileUpload['GeneoFileUpload']['modified'],
						'issue_date' => $geneoFileUpload['GeneoFileUpload']['issue_date'],
						'link' => Router::url($link)
					)
				)
			);
		}	
		return $results;
	}

/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		$collection = $db->selectCollection('geneo_file_uploads');
		// remove unnecessary fields from results and add score
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "status_id" => 1,
		  "description" => 1,
		  "draft_title" => 1,
		  "version" => 1,
		  "name" => 1,
		  "created" => 1,
		  "modified" => 1,
		  "issue_date" => 1,
		  "geneo_file_upload_collection_id" => 1,
		  'score' => ['$meta' => 'textScore']
		);
		// full text search for ALL items (including those outside of the user's scope e.g. in a different location)
		$fileUploads = iterator_to_array($collection->find(['$text' => ['$search' => $term]], $params)); 

		// 1. Get user_id 
		$user_id = AuthComponent::user('id');
		// 2. Get all containers for that user_id: 
		$geneo_companies_user_id = ClassRegistry::init('GeneoCompaniesUser')->find('all', array(
        	'conditions' => array('user_id' => $user_id)))[0]['GeneoCompaniesUser']['id'];
		$containers = ClassRegistry::init('GeneoCompanyUserContainer')->find('all', array(
        	'conditions' => array('geneo_companies_user_id' => $geneo_companies_user_id)));
		// 3. Find all process flows and process flow collections inside the containers that the user can access 
		$availableFileUploadsIds = array(); 		
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableFileUploadsIds = array_merge($availableFileUploadsIds, $this->getFileUploadsIdsInContainer($model, $model_id));
		}
		// 4. For each search result, check if its id or collection id == foreign_key of each container, if it is, add it to a new results array to be returned
		$results = array();
		$availableFileUploadsIds = array_unique($availableFileUploadsIds); 
		foreach ($fileUploads as $fileUpload) {
			if((in_array($fileUpload['id'], $availableFileUploadsIds) or in_array($fileUpload['geneo_file_upload_collection_id'], $availableFileUploadsIds)) 
				and ($fileUpload['status_id'] != GeneoAuth::$statusAll) and ($fileUpload['status_id'] != GeneoAuth::$statusPrevious)){
				$link = array(
							'controller' => 'geneo_file_uploads',
							'action' => 'view',
							$fileUpload['id']
						);
				$type = 'File Upload';
				$number = ClassRegistry::init('GeneoFileUploadCollection')->find('first', 
					array('conditions'=> array('id'=> $fileUpload['geneo_file_upload_collection_id'])))['GeneoFileUploadCollection']['document_number'];
				$number = sprintf("%'06s", $number);
				if($fileUpload['issue_date'] != null){
					$fileUpload['issue_date'] = date('Y-m-d h:i:s', $fileUpload['issue_date']->sec);
				}
				$results[] = array(
						'hit' => $type,
						'score' => str_replace('(float)','',$fileUpload['score']),
						'documents' => array(
							$type => array(
								'type' => $type,
								'number' => $number,
								'id' => $fileUpload['id'],
								'status' => GeneoAuth::getName($fileUpload['status_id']),
								'status_id' => str_replace('(int)','',$fileUpload['status_id']),
								'name' => $fileUpload['name'],
								'description' => $fileUpload['description'],
								'draft_title' => $fileUpload['draft_title'],
								'version' => $fileUpload['version'],
								'created' => date('Y-m-d h:i:s', $fileUpload['created']->sec),
								'modified' => date('Y-m-d h:i:s', $fileUpload['modified']->sec),
								'issue_date' => $fileUpload['issue_date'],
								'link' => Router::url($link)
							)
						)
					);
			}
		}
		// sort results based on full text score
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		$this->setDataSource('default');
		return $results;
	}

/**
 * Get IDs for all visual aids and visual aid collections inside a container and sub containers (department, 
 * location or work space)
 * @param 	string 			$model 			The container's model type e.g. 'GeneoDepartment'
 * @param 	string 			$container_id 	The container's id
 * @return 	array 			$results 		an array with VisualAid and VisualAidCollection IDs
 */
	public function getFileUploadsIdsInContainer($model, $container_id){
		$model_options = ['GeneoFileUpload', 'GeneoFileUploadCollection'];
		$container_ids = array($container_id);
		$child_containers = ClassRegistry::init($model)->children($container_id);
		
		if($child_containers){
			foreach ($child_containers as $child_container) {
				$child_container_id = $child_container[$model]['id'];
				array_push($container_ids, $child_container_id);
			}
		}
		$modelsRelation = $model . 'sRelation';
		$model_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', str_replace('Geneo', '', $model)));
		$id_index = 'geneo_' . $model_name . '_id';	
		$containerRelations = ClassRegistry::init($modelsRelation)->find('all', array(
        	'conditions' => array($id_index => $container_ids, 'model'=> $model_options)));

		$results = array();
		foreach ($containerRelations as $containerRelation) {
			$modelRelations = array_keys($containerRelation)[0];
			array_push($results, $containerRelation[$modelRelations]['foreign_key']);
		}
		return $results;
	}