<?php

	public $uses = array(
		'GeneoStandard',
		'GeneoJes',
		'GeneoStep',
		// added missing models:		
		'GeneoProcessFlow',
		'VisualAid',
		'GeneoFileUpload'
	);

	public function index() {

		// Code trimmed 

		$this->GeneoStandard->cacheQueries = false;
		$this->GeneoJes->cacheQueries = false;
		$this->GeneoStep->cacheQueries = false;
		// added missing models:
		$this->GeneoProcessFlow->cacheQueries = false;
		$this->VisualAid->cacheQueries = false;
		$this->GeneoFileUpload->cacheQueries = false;

		// models use new mongo search: 
		$steps = $this->GeneoStep->getSearchDataMongo($query);	
		$preResults = array_merge(
			$this->GeneoStandard->getSearchDataMongo($query),
 			$this->GeneoJes->getSearchDataMongo($query),
 			$steps,
			$this->GeneoProcessFlow->getSearchDataMongo($query),
			$this->VisualAid->getSearchDataMongo($query),
			$this->GeneoFileUpload->getSearchDataMongo($query) 

		);

		// Code trimmed 
	}
