<?php
/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		$collection = $db->selectCollection('geneo_standards');
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "type" => 1,
		  "status_id" => 1,
		  "description" => 1,
		  "draft_title" => 1,
		  "version" => 1,
		  "name" => 1,
		  "created" => 1,
		  "modified" => 1,
		  "issue_date" => 1,
		  "geneo_standard_collection_id" => 1,
		  'score' => ['$meta' => 'textScore']
		);
		// 1. Perform full text search on standards
		$standards = iterator_to_array($collection->find(['$text' => ['$search' => $term]], $params)); 
		$containers = $this->getUserContainers();
		$availableStandardsIds = array();	
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableStandardsIds = array_merge($availableStandardsIds, $this->getStandardIdsInContainer($model, $model_id));
		}
		$availableStandardsIds = array_unique($availableStandardsIds);
		$results = array();
		foreach ($standards as $standard) {
		    // 2. Filter out 
			if((in_array($standard['id'], $availableStandardsIds) or in_array($standard['geneo_standard_collection_id'], $availableStandardsIds)) 
				and ($standard['status_id'] != GeneoAuth::$statusAll) and ($standard['status_id'] != GeneoAuth::$statusPrevious)){
				$link = array(
							'controller' => 'geneo_standards',
							'action' => 'view',
							$standard['id']
						);
				$type = Term::get($standard['type']);
				$number = ClassRegistry::init('GeneoStandardCollection')->find('first', 
					array('conditions'=> array('id'=> $standard['geneo_standard_collection_id'])))['GeneoStandardCollection']['document_number'];
				$number = sprintf("%'06s", $number);
				if($standard['issue_date'] != null){
					$standard['issue_date'] = date('Y-m-d h:i:s', $standard['issue_date']->sec);
				}
				// 3. Format the results 
				$results[] = array(
						'hit' => $type,
						'score' => str_replace('(float)','',$standard['score']),
						'documents' => array(
							$type => array(
								'type' => ucwords(Term::get(sprintf('%s_full', $standard['type']))),
								'number' => $number,
								'id' => $standard['id'],
								'status' => GeneoAuth::getName($standard['status_id']),
								'status_id' => str_replace('(int)','',$standard['status_id']),
								'name' => $standard['name'],
								'description' => $standard['description'],
								'draft_title' => $standard['draft_title'],
								'version' => $standard['version'],
								'created' => date('Y-m-d h:i:s', $standard['created']->sec),
								'modified' => date('Y-m-d h:i:s', $standard['modified']->sec),
								'issue_date' => $standard['issue_date'],
								'link' => Router::url($link)
							)
						)
					);
			}
		}
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		$this->setDataSource('default');
		return $results;
	}


/**
 * Get IDs for all standards and standard collections inside a container and sub containers (department, location or work space)
 * @param 	string 			$model 			The container's model type e.g. 'GeneoDepartment'
 * @param 	string 			$container_id 	The container's id
 * @return 	array 			$results 		an array with GeneoStandard and GeneoStandardCollection IDs
 */
	public function getStandardIdsInContainer($model, $container_id){
		$model_options = ['GeneoStandard', 'GeneoStandardCollection'];
		$container_ids = array($container_id);
		$child_containers = ClassRegistry::init($model)->children($container_id);
		
		if($child_containers){
			foreach ($child_containers as $child_container) {
				$child_container_id = $child_container[$model]['id'];
				array_push($container_ids, $child_container_id);
			}
		}
		$modelsRelation = $model . 'sRelation';
		$model_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', str_replace('Geneo', '', $model)));
		$id_index = 'geneo_' . $model_name . '_id';	
		$containerRelations = ClassRegistry::init($modelsRelation)->find('all', array(
        	'conditions' => array($id_index => $container_ids, 'model'=> $model_options)));

		$results = array();
		foreach ($containerRelations as $containerRelation) {
			$modelRelations = array_keys($containerRelation)[0];
			array_push($results, $containerRelation[$modelRelations]['foreign_key']);
		}
		return $results;
	}
/**
 * Get all containers for logged in user container (department, location or work space)
 * @return 	array 			$containers 	
 */
	public function getUserContainers(){
		$user_id = AuthComponent::user('id');
		$geneo_companies_user_id = ClassRegistry::init('GeneoCompaniesUser')->find('all', array(
        	'conditions' => array('user_id' => $user_id)))[0]['GeneoCompaniesUser']['id'];
		$containers = ClassRegistry::init('GeneoCompanyUserContainer')->find('all', array(
        	'conditions' => array('geneo_companies_user_id' => $geneo_companies_user_id)));
		return $containers;

	}

/**
 * Format a Standard Collection document number
 * @return 	int 	$standardNumber 	
 */
	public function formatNumber($standardCollectionId){
		$standardNumber = $this->GeneoStandardCollection->find('first', 
					array('conditions'=> array('id'=> $standardCollectionId)))['GeneoStandardCollection']['document_number'];
		$standardNumber = sprintf("%'06s", $standardNumber);
		return $standardNumber;

	}	