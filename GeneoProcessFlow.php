<?php

/**
 * Accepts a search term and returns results for front-end searches
 *
 * @param string $term the term to search for
 * @param array $conditions additional conditions
 * @return array Search results
 */
	public function getSearchData($term, $conditions = array()) {
		$this->setDataSource('default');
		$containers = array(
			'GeneoDepartment',
			'GeneoLocation',
			'GeneoWorkSpace'
		);

		$processFlows = array();

		$searchTerm = $this->getDataSource()->getConnection()->quote($term);

		foreach ($containers as $container) {
			foreach (GeneoAuth::$statuses as $key => $status) {
				if ($key == GeneoAuth::$statusAll || $key == GeneoAuth::$statusPrevious) {
					continue; 
				}
				$options = $this->paginateOptions($key, $container, false);

				if ($options == false) {
					continue;
				}

				$fullText = "MATCH(GeneoProcessFlow.name, GeneoProcessFlow.description, GeneoProcessFlow.draft_title) AGAINST({$searchTerm} IN BOOLEAN MODE)";

				$this->virtualFields['fulltext_score'] = $fullText;

				$number = ltrim(substr($term, 0, -1), '0');

				if (is_numeric($number)) {
					$options['conditions'][] = array(
						'OR' => array(
							$fullText,
							'GeneoProcessFlowCollection.document_number' => $number
						)
					);
				} else {
					$options['conditions'][] = $fullText;
				}

				$options['fields'] = array(
					'GeneoProcessFlow.id',
					'GeneoProcessFlow.status_id',
					'GeneoProcessFlow.name',
					'GeneoProcessFlow.description',
					'GeneoProcessFlow.draft_title',
					'GeneoProcessFlow.version',
					'GeneoProcessFlow.created',
					'GeneoProcessFlow.modified',
					'GeneoProcessFlow.issue_date',
					'GeneoProcessFlow.fulltext_score',
					'GeneoProcessFlow.geneo_process_flow_collection_id',
					'GeneoProcessFlowCollection.document_number',
				);

				$options['limit'] = 20;

				$processFlows = array_merge($processFlows, $this->find('all', $options));
			}
		}

		$results = array();

		foreach ($processFlows as $processFlow) {

			$number = sprintf("%'06s", $processFlow['GeneoProcessFlowCollection']['document_number']);

			$link = array(
				'controller' => 'geneo_process_flows',
				'action' => 'view',
				$processFlow['GeneoProcessFlow']['id']
			);

			$results[] = array(
				'hit' => 'Process Flow',
				'score' => $processFlow['GeneoProcessFlow']['fulltext_score'],
				'documents' => array(
					'Process Flow' => array(
						'type'=> 'Process Flow',
						'number' => $number,
						'id' => $processFlow['GeneoProcessFlow']['id'],
						'status' => GeneoAuth::getName($processFlow['GeneoProcessFlow']['status_id']),
						'status_id' => $processFlow['GeneoProcessFlow']['status_id'],
						'name' => $processFlow['GeneoProcessFlow']['name'],
						'description' => $processFlow['GeneoProcessFlow']['description'],
						'draft_title' => $processFlow['GeneoProcessFlow']['draft_title'],
						'version' => $processFlow['GeneoProcessFlow']['version'],
						'created' => $processFlow['GeneoProcessFlow']['created'],
						'modified' => $processFlow['GeneoProcessFlow']['modified'],
						'issue_date' => $processFlow['GeneoProcessFlow']['issue_date'],
						'link' => Router::url($link)
					)
				)
			);
		}

		return $results;
	}

/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		// $mongo = new Mongo('localhost:27017'); // deprecated
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		$collection = $db->selectCollection('geneo_process_flows');
		// remove unnecessary fields from results and add score
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "status_id" => 1,
		  "description" => 1,
		  "draft_title" => 1,
		  "version" => 1,
		  "name" => 1,
		  "created" => 1,
		  "modified" => 1,
		  "issue_date" => 1,
		  "geneo_process_flow_collection_id" => 1,
		  'score' => ['$meta' => 'textScore']
		);
		// full text search for ALL items (including those outside of the user's scope e.g. in a different location)
		$processFlows = iterator_to_array($collection->find(['$text' => ['$search' => $term]], $params)); 

		// 1. Get user_id 
		$user_id = AuthComponent::user('id');
		// 2. Get all containers for that user_id: 
		$geneo_companies_user_id = ClassRegistry::init('GeneoCompaniesUser')->find('all', array(
        	'conditions' => array('user_id' => $user_id)))[0]['GeneoCompaniesUser']['id'];
		$containers = ClassRegistry::init('GeneoCompanyUserContainer')->find('all', array(
        	'conditions' => array('geneo_companies_user_id' => $geneo_companies_user_id)));
		// 3. Find all process flows and process flow collections inside the containers that the user can access 
		$availableProcessFlowsIds = array(); 		
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableProcessFlowsIds = array_merge($availableProcessFlowsIds, $this->getProcessFlowIdsInContainer($model, $model_id));
		}
		// 4. For each search result, check if its id or collection id == foreign_key of each container, if it is, add it to a new results array to be returned
		$results = array();
		$availableProcessFlowsIds = array_unique($availableProcessFlowsIds); 
		foreach ($processFlows as $processFlow) {
			if((in_array($processFlow['id'], $availableProcessFlowsIds) or in_array($processFlow['geneo_process_flow_collection_id'], $availableProcessFlowsIds)) 
				and ($processFlow['status_id'] != GeneoAuth::$statusAll) and ($processFlow['status_id'] != GeneoAuth::$statusPrevious)){
				// editing results format
				$link = array(
							'controller' => 'geneo_process_flows',
							'action' => 'view',
							$processFlow['id']
						);
				$type = 'Process Flow';
				$number = ClassRegistry::init('GeneoProcessFlowCollection')->find('first', 
					array('conditions'=> array('id'=> $processFlow['geneo_process_flow_collection_id'])))['GeneoProcessFlowCollection']['document_number'];
				$number = sprintf("%'06s", $number);
				if($processFlow['issue_date'] != null){
					$processFlow['issue_date'] = date('Y-m-d h:i:s', $processFlow['issue_date']->sec);
				}
				$results[] = array(
						'hit' => $type,
						'score' => str_replace('(float)','',$processFlow['score']),
						'documents' => array(
							$type => array(
								'type' => $type,
								'number' => $number,
								'id' => $processFlow['id'],
								'status' => GeneoAuth::getName($processFlow['status_id']),
								'status_id' => str_replace('(int)','',$processFlow['status_id']),
								'name' => $processFlow['name'],
								'description' => $processFlow['description'],
								'draft_title' => $processFlow['draft_title'],
								'version' => $processFlow['version'],
								'created' => date('Y-m-d h:i:s', $processFlow['created']->sec),
								'modified' => date('Y-m-d h:i:s', $processFlow['modified']->sec),
								'issue_date' => $processFlow['issue_date'],
								'link' => Router::url($link)
							)
						)
					);
			}
		}
		// sort results based on full text score
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		$this->setDataSource('default');
		return $results;
	}

/**
 * Get IDs for all process flows and process flow collections inside a container and sub containers (department, 
 * location or work space)
 * @param 	string 			$model 			The container's model type e.g. 'GeneoDepartment'
 * @param 	string 			$container_id 	The container's id
 * @return 	array 			$results 		an array with GeneoProcessFlow and GeneoProcessFlowCollection IDs
 */
	public function getProcessFlowIdsInContainer($model, $container_id){
		$model_options = ['GeneoProcessFlow', 'GeneoProcessFlowCollection'];
		$container_ids = array($container_id);
		$child_containers = ClassRegistry::init($model)->children($container_id);
		
		if($child_containers){
			foreach ($child_containers as $child_container) {
				$child_container_id = $child_container[$model]['id'];
				array_push($container_ids, $child_container_id);
			}
		}
		$modelsRelation = $model . 'sRelation';
		$model_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', str_replace('Geneo', '', $model)));
		$id_index = 'geneo_' . $model_name . '_id';	
		$containerRelations = ClassRegistry::init($modelsRelation)->find('all', array(
        	'conditions' => array($id_index => $container_ids, 'model'=> $model_options)));

		$results = array();
		foreach ($containerRelations as $containerRelation) {
			$modelRelations = array_keys($containerRelation)[0];
			array_push($results, $containerRelation[$modelRelations]['foreign_key']);
		}
		return $results;
	}