<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStep->getSearchData('Step 11');
		$expected_result = $this->GeneoStep->find('first', array('conditions' => array('GeneoStep.name' => 'step 11')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Step']['id']));
			}
		$this->assertTrue(in_array($expected_result['GeneoStep']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStep->getSearchData('very interesting description');
		$expected_result = $this->GeneoStep->find('first', array(
			'conditions' => array(
				'GeneoStep.description' => 'This is a very interesting description for a step'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Step']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoStep']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStep->getSearchData('very interesting draft title');
		$expected_result = $this->GeneoStep->find('first', array('conditions' => array('GeneoStep.draft_title' => 'This is a very interesting draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Step']['id']));
		}
		$this->assertTrue(in_array($expected_result['Geneo Step']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->GeneoStep->getSearchData('job element sheet');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Geneo Step']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Geneo Step']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->GeneoStep->getSearchData('This step is for location geneo-7-a');
		$step_out_of_location = $this->GeneoStep->find('first', array('conditions' => array('GeneoStep.description' => 'This step is for location geneo-7-a')));
		$this->assertFalse(in_array($step_out_of_location, $search_out_of_location));
	}


	
