<?php

/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		// search first for standards
		$collection = $db->selectCollection('geneo_jeses');
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "type" => 1,
		  "status_id" => 1,
		  "description" => 1,
		  "draft_title" => 1,
		  "version" => 1,
		  "name" => 1,
		  "created" => 1,
		  "modified" => 1,
		  "issue_date" => 1,
		  "geneo_jes_collection_id" => 1,
		  "origin_geneo_jes_id" => 1,
		  'score' => ['$meta' => 'textScore'],
		);
		// Perform full text search on jeses and then add the standard that they belong to 
		$jeses = iterator_to_array($collection->find(['$text' => ['$search' => $term]],$params));
		$standard_steps = array();
		// join with geneo standard steps
		$collection = $db->selectCollection('geneo_standards_steps');
		foreach ($jeses as $jes) {
			$params = array('foreign_key' => $jes['id']);
			$found_standard_steps = iterator_to_array($collection->find($params));
			$standard_steps += $found_standard_steps;
		}
		// join with geneo standards
		$collection = $db->selectCollection('geneo_standards');
		$duplicated_jeses = array(); // jeses that are shared across many standards
		foreach ($jeses as &$jes) {
			$jes['linked_standard'] = '';
			// find the standard it belongs to and insert it
			foreach ($standard_steps as $standard_step) {
				if($jes['id'] == $standard_step['foreign_key']){
					$params = array('id' => $standard_step['geneo_standard_id']);
					$found_standard = iterator_to_array($collection->find($params));
					foreach ($found_standard as $standard) {
						if(!empty($found_standard)){
							if($jes['linked_standard'] == ''){
								$jes['linked_standard'] = $found_standard;	
							}else{
								// add that jes again
								$duplicated_jes = $jes;
								$duplicated_jes['linked_standard'] = $found_standard;
								$duplicated_jeses[] = $duplicated_jes;
							}															
						}
					}						
				}
			}
		}
		$jeses = array_merge($jeses, $duplicated_jeses);
		$containers = $this->GeneoStandardsStep->GeneoStandard->getUserContainers();
		// Find all jeses and jes collections inside the containers that the user can access 
		 $availableStandardsIds = array(); 		
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableStandardsIds = array_merge($availableStandardsIds, $this->GeneoStandardsStep->GeneoStandard->getStandardIdsInContainer($model, $model_id));
		}
		// For each search result, check if its id or collection id == foreign_key of each container, if it is, add it to a new results array to be returned
		$results = array();
		$availableStandardsIds = array_unique($availableStandardsIds);
		foreach ($jeses as $jes) {
			// standard that contains the jes
			$jesStandard = '';
			if($jes['linked_standard'] != ''){
				$jesStandard = reset($jes['linked_standard']);
			}
			if($jesStandard != '' and (in_array($jesStandard['id'], $availableStandardsIds) or in_array($jesStandard['geneo_standard_collection_id'], $availableStandardsIds)) 
				and ($jesStandard['status_id'] != GeneoAuth::$statusAll) and ($jesStandard['status_id'] != GeneoAuth::$statusPrevious)){
				$link = array(
							'controller' => 'geneo_jeses',
							'action' => 'view',
							$jes['id']
						);
				$type = Term::get($jes['type']);
				$jesNumber = ClassRegistry::init('GeneoJesCollection')->find('first', 
					array('conditions'=> array('id'=> $jes['geneo_jes_collection_id'])))['GeneoJesCollection']['document_number'];
				$jesNumber = sprintf("%'06s", $jesNumber);
				if($jesStandard['issue_date'] != null){
					$jesStandard['issue_date'] = date('Y-m-d h:i:s', $jesStandard['issue_date']->sec);
				}
				$standardLink = array(
					'controller' => 'geneo_standards',
					'action' => 'view',
					$jesStandard['id']
					);
				$standardNumber = ClassRegistry::init('GeneoStandardCollection')->find('first', 
					array('conditions'=> array('id'=> $standard['geneo_standard_collection_id'])))['GeneoStandardCollection']['document_number'];
				$standardNumber = sprintf("%'06s", $standardNumber);
				$type = Term::get($jesStandard['type']);
				$results[] = array(
						'hit' => 'JES',
						'score' => str_replace('(float)','',$jes['score']),
						'documents' => array(
							$type => array(
								'type' => ucwords(Term::get(sprintf('%s_full', $jesStandard['type']))),
								'number' => $standardNumber,
								'id' => $jesStandard['id'],
								'status' => GeneoAuth::getName($jesStandard['status_id']),
								'status_id' => str_replace('(int)','',$jesStandard['status_id']),
								'name' => $jesStandard['name'],
								'description' => $jesStandard['description'],
								'draft_title' => $jesStandard['draft_title'],
								'version' => $jesStandard['version'],
								'created' => date('Y-m-d h:i:s', $jesStandard['created']->sec),
								'modified' => date('Y-m-d h:i:s', $jesStandard['modified']->sec),
								'issue_date' => $jesStandard['issue_date'],
								'link' => Router::url($standardLink),
							)
							,'JES' => array(
									'type' => ucwords(Term::get(sprintf('%s_full', $jes['type']))),
									'number' => $jesNumber,
									'id' => $jes['id'],
									'status' => GeneoAuth::getName($jes['status_id']),
									'status_id' => str_replace('(int)','',$jes['status_id']),
									'name' => $jes['name'],
									'description' => $jes['description'],
									'draft_title' => $jes['draft_title'],
									'version' => $jes['version'],
									'created' => $jes['created'],
									'modified' => $jes['modified'],
									'issue_date' => $jes['issue_date'],
									'link' => Router::url($standardLink)
								)
						)
					);
			}
		}
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		return $results;
	}

/**
 * Get IDs for all jeses and jes collections inside a container and sub containers (department, location or work space)
 * @param 	string 			$model 			The container's model type e.g. 'GeneoDepartment'
 * @param 	string 			$container_id 	The container's id
 * @return 	array 			$results 		an array with GeneoJes and GeneoJesCollection IDs
 */
	public function getJesIdsInContainer($model, $container_id){
		$model_options = ['GeneoJes', 'GeneoJesCollection'];
		$container_ids = array($container_id);
		$child_containers = ClassRegistry::init($model)->children($container_id);
		
		if($child_containers){
			foreach ($child_containers as $child_container) {
				$child_container_id = $child_container[$model]['id'];
				array_push($container_ids, $child_container_id);
			}
		}
		$modelsRelation = $model . 'sRelation';
		$model_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', str_replace('Geneo', '', $model)));
		$id_index = 'geneo_' . $model_name . '_id';	
		$containerRelations = ClassRegistry::init($modelsRelation)->find('all', array(
        	'conditions' => array($id_index => $container_ids, 'model'=> $model_options)));

		$results = array();
		foreach ($containerRelations as $containerRelation) {
			$modelRelations = array_keys($containerRelation)[0];
			array_push($results, $containerRelation[$modelRelations]['foreign_key']);
		}
		return $results;
	}
/**
 * Format a Standard Collection document number
 * @return 	int 	$standardNumber 	
 */
	public function formatNumber($jesCollectionId){
		$standardNumber = $this->GeneoJesCollection->find('first', 
					array('conditions'=> array('id'=> $jesCollectionId)))['GeneoJesCollection']['document_number'];
		$standardNumber = sprintf("%'06s", $standardNumber);
		return $standardNumber;

	}