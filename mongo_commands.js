use demo
db.geneo_standards.createIndex({ name: "text", description: "text", draft_title: "text"})
db.geneo_jeses.createIndex({ name: "text", description: "text", draft_title: "text"})
db.geneo_steps.createIndex({ name: "text", description: "text"})
db.geneo_process_flows.createIndex({ name: "text", description: "text", draft_title: "text"})
db.visual_aids.createIndex({ name: "text", description: "text", draft_title: "text"})
db.geneo_file_uploads.createIndex({ name: "text", description: "text", draft_title: "text"})

