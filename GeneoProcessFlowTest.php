<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoProcessFlow->getSearchData('process flow 2');
		$expected_result = $this->GeneoProcessFlow->find('first', array('conditions' => array('GeneoProcessFlow.name' => 'process flow 2')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Process Flow']['id']));
			}
		$this->assertTrue(in_array($expected_result['GeneoProcessFlow']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoProcessFlow->getSearchData('very interesting description');
		$expected_result = $this->GeneoProcessFlow->find('first', array(
			'conditions' => array(
				'GeneoProcessFlow.description' => 'This is a very interesting description for a process flow'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Process Flow']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoProcessFlow']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoProcessFlow->getSearchData('very interesting draft title');
		$expected_result = $this->GeneoProcessFlow->find('first', array('conditions' => array('GeneoProcessFlow.draft_title' => 'This is a very interesting draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Process Flow']['id']));
		}
		$this->assertTrue(in_array($expected_result['Geneo Process Flow']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->GeneoProcessFlow->getSearchData('process flow');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Geneo Process Flow']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Geneo Process Flow']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->GeneoProcessFlow->getSearchData('This process flow is for location geneo-7-a');
		$process_flow_out_of_location = $this->GeneoProcessFlow->find('first', array('conditions' => array('GeneoProcessFlow.description' => 'This process flow is for location geneo-7-a')));
		$this->assertFalse(in_array($process_flow_out_of_location, $search_out_of_location));
	}


	
