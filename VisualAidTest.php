<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->VisualAid->getSearchData('Visual Aid 13');
		$expected_result = $this->VisualAid->find('first', array('conditions' => array('VisualAid.name' => 'Visual Aid 13')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Visual Aid']['id']));
			}
		$this->assertTrue(in_array($expected_result['VisualAid']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->VisualAid->getSearchData('dependent visual aid');
		$expected_result = $this->VisualAid->find('first', array(
			'conditions' => array(
				'VisualAid.description' => 'This is a dependent visual aid for geneo jes jes-geneo-shared-9 collection'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Visual Aid']['id']));
		}
		$this->assertTrue(in_array($expected_result['VisualAid']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->VisualAid->getSearchData('very interesting draft title');
		$expected_result = $this->VisualAid->find('first', array('conditions' => array('VisualAid.draft_title' => 'This is a very interesting 				draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Visual Aid']['id']));
		}
		$this->assertTrue(in_array($expected_result['VisualAid']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->VisualAid->getSearchData('visual aid');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Visual Aid']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Visual Aid']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->VisualAid->getSearchData('This visual aid is for location geneo-7-a');
		$visual_aid_out_of_location = $this->VisualAid->find('first', array('conditions' => array('VisualAid.description' => 'This visual aid 				is for location geneo-7-a')));
		$this->assertFalse(in_array($visual_aid_out_of_location, $search_out_of_location));
	}


	
