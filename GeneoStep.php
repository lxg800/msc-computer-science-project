<?php

/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		$collection = $db->selectCollection('geneo_steps');
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "geneo_jes_id" => 1,
		  "description" => 1,
		  "name" => 1,
		  "va" => 1,
		  "nva" => 1,
		  "nnva" => 1,
		  "created" => 1,
		  "modified" => 1,
		  'score' => ['$meta' => 'textScore'],
		);
		$steps = iterator_to_array($collection->find(['$text' => ['$search' => $term]],$params));
		$duplicated_steps = $standard_steps = array();
		foreach ($steps as &$step) {
			$linked_jes = $this->GeneoJes->find('first', array('conditions' => array('GeneoJes.id'=>$step['geneo_jes_id'])));
			$step['linked_jes'] = '';
			$step['linked_standard'] = '';
			// add jes
			if($linked_jes){
				$step['linked_jes'] = reset($linked_jes);
				// add standard - each jes can appear in many standards
				$linked_jes['linked_standard'] = '';
				$standard_steps = $this->GeneoJes->GeneoStandardsStep->find('all', 
					array('conditions' => array('foreign_key' => $linked_jes['GeneoJes']['id'])));
				foreach ($standard_steps as $standard_step) {
					$step['standard_step_id'] = $standard_step['GeneoStandardsStep']['id'];
					$standard_step = reset($standard_step);
					$found_standard = $this->GeneoJes->GeneoStandardsStep->GeneoStandard->find('first', 
						array('conditions'=>array('GeneoStandard.id' => $standard_step['geneo_standard_id'])));
					if(!$found_standard){
						continue;
					}
					$found_standard = $found_standard['GeneoStandard'];					
					if($step['linked_standard'] == ''){
						$step['linked_standard'] = $found_standard;	
					}else{
						$duplicated_step = $step;
						$duplicated_step['linked_standard'] = $found_standard;
						$duplicated_steps[] = $duplicated_step;
					}
				}
			}			
		}
		$steps = array_merge($steps, $duplicated_steps);
		$containers = $this->GeneoJes->GeneoStandardsStep->GeneoStandard->getUserContainers();
		// 3. Find all jeses and jes collections inside the containers that the user can access 
		$availableStandardsIds = array(); 		
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableStandardsIds = array_merge($availableStandardsIds, $this->GeneoJes->GeneoStandardsStep->GeneoStandard->getStandardIdsInContainer($model, $model_id));
		}
		// 4. For each search result, check if its standard id is for an available standard
		$results = array();
		$availableStandardsIds = array_unique($availableStandardsIds);
		foreach ($steps as $step) {
			// standard that contains the step
			$stepStandard = '';
			if($step['linked_standard'] != ''){
				$stepStandard = $step['linked_standard'];
			}
			if($stepStandard != '' and (in_array($stepStandard['id'], $availableStandardsIds) or in_array($stepStandard['geneo_standard_collection_id'], $availableStandardsIds)) 
				and ($stepStandard['status_id'] != GeneoAuth::$statusAll) and ($stepStandard['status_id'] != GeneoAuth::$statusPrevious)){
				$type = Term::get($stepStandard['type']);
				$standardLink = array(
					'controller' => 'geneo_standards',
					'action' => 'view',
					$stepStandard['id']
					);
				$standardNumber = $this->GeneoJes->GeneoStandardsStep->GeneoStandard->formatNumber($stepStandard['geneo_standard_collection_id']);
				$stepJes = $step['linked_jes'];
				$jesNumber = $this->GeneoJes->formatNumber($stepJes['geneo_jes_collection_id']);
				$standard_step_id = $step['standard_step_id'];
				$jesLink = array(
					'controller' => 'geneo_jeses',
					'action' => 'view',
					$stepJes['id'],
					'geneo_standards_step_id' => $standard_step_id
				);
				$results[] = array(
						'hit' => 'Step',
						'score' => str_replace('(float)','',$step['score']),
						'documents' => array(
							$type => array(
								'type' => ucwords(Term::get(sprintf('%s_full', $stepStandard['type']))),
								'number' => $standardNumber,
								'id' => $stepStandard['id'],
								'status' => GeneoAuth::getName($stepStandard['status_id']),
								'status_id' => str_replace('(int)','',$stepStandard['status_id']),
								'name' => $stepStandard['name'],
								'description' => $stepStandard['description'],
								'draft_title' => $stepStandard['draft_title'],
								'version' => $stepStandard['version'],
								'created' => $stepStandard['created'],
								'modified' => $stepStandard['modified'],
								'issue_date' => $stepStandard['issue_date'],
								'link' => Router::url($standardLink),
							)
							,'JES' => array(
									'type' => ucwords(Term::get(sprintf('%s_full', $stepJes['type']))),
									'number' => $jesNumber,
									'id' => $stepJes['id'],
									'status' => GeneoAuth::getName($stepJes['status_id']),
									'status_id' => str_replace('(int)','',$stepJes['status_id']),
									'name' => $stepJes['name'],
									'description' => $stepJes['description'],
									'draft_title' => $stepJes['draft_title'],
									'version' => $stepJes['version'],
									'created' => $stepJes['created'],
									'modified' => $stepJes['modified'],
									'issue_date' => $stepJes['issue_date'],
									'link' => Router::url($standardLink)
								)
							,'Step' => array(
									'type' => 'Step',
									'id' => $step['id'],
									'name' => $step['name'],
									'description' => $step['description'],
									'va' => $step['va'],
									'nva' => $step['va'],
									'nnva' => $step['va'],
									'created' => $step['created'],
									'modified' => $step['modified'],
									'link' => Router::url($jesLink)
								)
						)
					);
			}
		}
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		return $results;
	}