<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStandard->getSearchData('Standard 9');
		$expected_result = $this->GeneoStandard->find('first', array('conditions' => array('GeneoStandard.name' => 'Standard 9')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Standard']['id']));
			}
		$this->assertTrue(in_array($expected_result['GeneoStandard']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStandard->getSearchData('dependent standard');
		$expected_result = $this->GeneoStandard->find('first', array(
			'conditions' => array(
				'GeneoStandard.description' => 'This is a dependent standard'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Standard']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoStandard']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoStandard->getSearchData('very interesting draft title');
		$expected_result = $this->GeneoStandard->find('first', array('conditions' => array('GeneoStandard.draft_title' => 'This is a very interesting draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Standard']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoStandard']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->GeneoStandard->getSearchData('Standard');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Geneo Standard']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Geneo Standard']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->GeneoStandard->getSearchData('This standard is for location geneo-7-a');
		$standard_out_of_location = $this->GeneoStandard->find('first', array('conditions' => array('GeneoStandard.description' => 'This standard is for location geneo-7-a')));
		$this->assertFalse(in_array($standard_out_of_location, $search_out_of_location));
	}


	
