<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoFileUpload->getSearchData('file upload 1');
		$expected_result = $this->GeneoFileUpload->find('first', array('conditions' => array('GeneoFileUpload.name' => 'file upload 1')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo File Upload']['id']));
			}
		$this->assertTrue(in_array($expected_result['GeneoFileUpload']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoFileUpload->getSearchData('very interesting description');
		$expected_result = $this->GeneoFileUpload->find('first', array(
			'conditions' => array(
				'GeneoFileUpload.description' => 'This is a very interesting description for a file upload'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo File Upload']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoFileUpload']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoFileUpload->getSearchData('very interesting draft title');
		$expected_result = $this->GeneoFileUpload->find('first', array('conditions' => array('GeneoFileUpload.draft_title' => 'This is a very interesting draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo File Upload']['id']));
		}
		$this->assertTrue(in_array($expected_result['Geneo File Upload']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->GeneoFileUpload->getSearchData('file upload');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Geneo File Upload']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Geneo File Upload']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->GeneoFileUpload->getSearchData('This file upload is for location geneo-7-a');
		$file_upload_out_of_location = $this->GeneoFileUpload->find('first', array('conditions' => array('GeneoFileUpload.description' => 'This file upload is for location geneo-7-a')));
		$this->assertFalse(in_array($file_upload_out_of_location, $search_out_of_location));
	}


	
