<?php
/**
 * Tests fetching some data using fulltextable
 * @return void
 */
	public function testGetSearchDataByName() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoJes->getSearchData('Job element sheet 9');
		$expected_result = $this->GeneoJes->find('first', array('conditions' => array('GeneoJes.name' => 'Job element sheet 3')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Jes']['id']));
			}
		$this->assertTrue(in_array($expected_result['GeneoJes']['id'], $results));
	}

	public function testGetSearchDataByDescription() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoJes->getSearchData('very interesting description');
		$expected_result = $this->GeneoJes->find('first', array(
			'conditions' => array(
				'GeneoJes.description' => 'This is a very interesting description for a job element sheet'
			)));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Jes']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoJes']['id'], $results));

	}

	public function testGetSearchDataByDraftTitle() {
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$search = $this->GeneoJes->getSearchData('very interesting draft title');
		$expected_result = $this->GeneoJes->find('first', array('conditions' => array('GeneoJes.draft_title' => 'This is a very interesting draft title')));
		$results = array();
		foreach($search as $result){
			$results = array_merge($results,array($result['documents']['Geneo Jes']['id']));
		}
		$this->assertTrue(in_array($expected_result['GeneoJes']['id'], $results));

	}

	public function testGetSearchDataCheckStatus() {
		// check that search results include documents with status = 1 (live) and status = 0 (draft) 
		Geneo::setUser('user-luisgarcia', 'company-geneo');
		$searchStatusCheck = $this->GeneoJes->getSearchData('job element sheet');
		$containsDraft = false;
		$containsLive = false;
		foreach($searchStatusCheck as $result){
			if($result['documents']['Geneo Jes']['status_id'] == '0'){
				$containsDraft = true;
			}
			if($result['documents']['Geneo Jes']['status_id'] == '1'){
				$containsLive = true;
			}
			if($containsDraft & $containsLive == true){
				break;
			}
		}
		$this->assertTrue($containsDraft);
		$this->assertTrue($containsLive);
	}

	public function testGetSearchDataCheckLocation() {
		// check that you can only see records within your location
	Geneo::setUser('user-jellehenkens', 'company-geneo'); // user can see: location-geneo-5-a-floor-1, location-geneo-6
		$search_out_of_location = $this->GeneoJes->getSearchData('This jes is for location geneo-7-a');
		$jes_out_of_location = $this->GeneoJes->find('first', array('conditions' => array('GeneoJes.description' => 'This jes is for location geneo-7-a')));
		$this->assertFalse(in_array($jes_out_of_location, $search_out_of_location));
	}


	
