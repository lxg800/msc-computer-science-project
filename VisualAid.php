<?php

/**
 * Accepts a search term and returns results for front-end searches
 *
 * @param string $term the term to search for
 * @param array $conditions additional conditions
 * @return array Search results
 */
	public function getSearchData($term, $conditions = array()) {
		$this->setDataSource('default');
		$containers = array(
			'GeneoDepartment',
			'GeneoLocation',
			'GeneoWorkSpace'
		);

		$visualAids = array();
		$searchTerm = $this->getDataSource()->getConnection()->quote($term);

		foreach ($containers as $container) {
			foreach (GeneoAuth::$statuses as $key => $status) {
				if ($key == GeneoAuth::$statusAll || $key == GeneoAuth::$statusPrevious) {
					continue;
				}

				$options = $this->paginateOptions($key, $container, false);
				if ($options == false) {
					continue;
				}

				$fullText = "MATCH(VisualAid.name, VisualAid.description, VisualAid.draft_title) AGAINST({$searchTerm} IN BOOLEAN MODE)";
				$this->virtualFields['fulltext_score'] = $fullText;
				$number = ltrim(substr($term, 0, -1), '0');

				if (is_numeric($number)) {
					$options['conditions'][] = array(
						'OR' => array(
							$fullText,
							'VisualAidCollection.document_number' => $number
						)
					);
				} else {
					$options['conditions'][] = $fullText;
				}
				
				$options['fields'] = array(
					'VisualAid.id',
					'VisualAid.status_id',
					'VisualAid.name',
					'VisualAid.description',
					'VisualAid.draft_title',
					'VisualAid.version',
					'VisualAid.created',
					'VisualAid.modified',
					'VisualAid.issue_date',
					'VisualAid.fulltext_score',
					'VisualAid.visual_aid_collection_id',
					'VisualAidCollection.document_number',
				);

				$options['limit'] = 20;
				
				$visualAids = array_merge($visualAids, $this->find('all', $options));

			}
		}

		$results = array();

		foreach ($visualAids as $visualAid) {
			$number = sprintf("%'06s", $visualAid['VisualAidCollection']['document_number']);
			$link = array(
				'controller' => 'visual_aids',
				'action' => 'view',
				$visualAid['VisualAid']['id']
			);
			$results[] = array(
				'hit' => 'Visual Aid',
				'score' => $visualAid['VisualAid']['fulltext_score'],
				'documents' => array(
					'Visual Aid' => array(
						'type' => 'Visual Aid',
						'number' => $number,
						'id' => $visualAid['VisualAid']['id'],
						'status' => GeneoAuth::getName($visualAid['VisualAid']['status_id']),
						'status_id' => $visualAid['VisualAid']['status_id'],
						'name' => $visualAid['VisualAid']['name'],
						'description' => $visualAid['VisualAid']['description'],
						'draft_title' => $visualAid['VisualAid']['draft_title'],
						'version' => $visualAid['VisualAid']['version'],
						'created' => $visualAid['VisualAid']['created'],
						'modified' => $visualAid['VisualAid']['modified'],
						'issue_date' => $visualAid['VisualAid']['issue_date'],
						'link' => Router::url($link)
					)
				)
			);
		}	
		return $results;
	}

/**
 * Accepts a search term and returns results for front-end searches
 * Connects to an alternative Mongodb NoSQL database for a faster 
 * search
 * @param string $term the term to search for
 * @return array Search results
 */
	public function getSearchDataMongo($term){
		$this->setDataSource('mongo');
		$mongo = new MongoClient('mongodb://localhost/');
		$db = $mongo->selectDB('demo');
		$collection = $db->selectCollection('visual_aids');
		$params = array(
		  "_id" => 0,
		  "id" => 1,
		  "status_id" => 1,
		  "description" => 1,
		  "draft_title" => 1,
		  "version" => 1,
		  "name" => 1,
		  "created" => 1,
		  "modified" => 1,
		  "issue_date" => 1,
		  "visual_aid_collection_id" => 1,
		  'score' => ['$meta' => 'textScore']
		);
		$visualAids = iterator_to_array($collection->find(['$text' => ['$search' => $term]], $params)); 

		// 1. Get user_id 
		$user_id = AuthComponent::user('id');
		// 2. Get all containers for that user_id: 
		$geneo_companies_user_id = ClassRegistry::init('GeneoCompaniesUser')->find('all', array(
        	'conditions' => array('user_id' => $user_id)))[0]['GeneoCompaniesUser']['id'];
		$containers = ClassRegistry::init('GeneoCompanyUserContainer')->find('all', array(
        	'conditions' => array('geneo_companies_user_id' => $geneo_companies_user_id)));
		// 3. Find all process flows and process flow collections inside the containers that the user can access 
		$availableVisualAidsIds = array(); 		
		foreach ($containers as $container) {
			$model = $container['GeneoCompanyUserContainer']['model'];
			$model_id = $container['GeneoCompanyUserContainer']['foreign_key'];
			$availableVisualAidsIds = array_merge($availableVisualAidsIds, $this->getVisualAidIdsInContainer($model, $model_id));
		}
		// 4. For each search result, check if its id or collection id == foreign_key of each container, if it is, add it to a new results array to be returned
		$results = array();
		$availableVisualAidsIds = array_unique($availableVisualAidsIds); 
		foreach ($visualAids as $visualAid) {
			if((in_array($visualAid['id'], $availableVisualAidsIds) or in_array($visualAid['visual_aid_collection_id'], $availableVisualAidsIds)) 
				and ($visualAid['status_id'] != GeneoAuth::$statusAll) and ($visualAid['status_id'] != GeneoAuth::$statusPrevious)){
				// editing results format
				$link = array(
							'controller' => 'visual_aid',
							'action' => 'view',
							$visualAid['id']
						);
				$type = 'Visual Aid';
				$number = ClassRegistry::init('VisualAidCollection')->find('first', 
					array('conditions'=> array('id'=> $visualAid['visual_aid_collection_id'])))['VisualAidCollection']['document_number'];
				$number = sprintf("%'06s", $number);
				if($visualAid['issue_date'] != null){
					$visualAid['issue_date'] = date('Y-m-d h:i:s', $visualAid['issue_date']->sec);
				}
				$results[] = array(
						'hit' => $type,
						'score' => str_replace('(float)','',$visualAid['score']),
						'documents' => array(
							$type => array(
								'type' => 'Visual Aid',
								'number' => $number,
								'id' => $visualAid['id'],
								'status' => GeneoAuth::getName($visualAid['status_id']),
								'status_id' => str_replace('(int)','',$visualAid['status_id']),
								'name' => $visualAid['name'],
								'description' => $visualAid['description'],
								'draft_title' => $visualAid['draft_title'],
								'version' => $visualAid['version'],
								'created' => date('Y-m-d h:i:s', $visualAid['created']->sec),
								'modified' => date('Y-m-d h:i:s', $visualAid['modified']->sec),
								'issue_date' => $visualAid['issue_date'],
								'link' => Router::url($link)
							)
						)
					);
			}
		}
		// sort results based on full text score
		usort($results, function($a, $b) {
			if ($a['score'] == $b['score']) {
				return 0;
			}
		    return $b['score'] > $a['score'] ? 1 : -1;
		});
		$this->setDataSource('default');
		return $results;
	}

/**
 * Get IDs for all visual aids and visual aid collections inside a container and sub containers (department, 
 * location or work space)
 * @param 	string 			$model 			The container's model type e.g. 'GeneoDepartment'
 * @param 	string 			$container_id 	The container's id
 * @return 	array 			$results 		an array with VisualAid and VisualAidCollection IDs
 */
	public function getVisualAidIdsInContainer($model, $container_id){
		$model_options = ['VisualAid', 'VisualAidCollection'];
		$container_ids = array($container_id);
		$child_containers = ClassRegistry::init($model)->children($container_id);
		
		if($child_containers){
			foreach ($child_containers as $child_container) {
				$child_container_id = $child_container[$model]['id'];
				array_push($container_ids, $child_container_id);
			}
		}
		$modelsRelation = $model . 'sRelation';
		$model_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', str_replace('Geneo', '', $model)));
		$id_index = 'geneo_' . $model_name . '_id';	
		$containerRelations = ClassRegistry::init($modelsRelation)->find('all', array(
        	'conditions' => array($id_index => $container_ids, 'model'=> $model_options)));

		$results = array();
		foreach ($containerRelations as $containerRelation) {
			$modelRelations = array_keys($containerRelation)[0];
			array_push($results, $containerRelation[$modelRelations]['foreign_key']);
		}
		return $results;
	}