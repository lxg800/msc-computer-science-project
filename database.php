<?php

	public $mongo = array(
		'datasource' => 'Mongodb.MongodbSource',
		'host' => 'localhost',
		'database' => 'demo',
		'port' => 27017,
		'prefix' => '',
		'persistent' => 'true'
	);

	public $test_mongo = array(
		'datasource' => 'Mongodb.MongodbSource',
		'database' => 'test_mongo',
		'host' => 'localhost',
		'port' => 27017,
	); 